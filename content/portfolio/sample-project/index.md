---
title: Erkin Company
description: This is the description of our sample project
date: "2019-05-02T19:47:09+02:00"
jobDate: 2021
work: [design, construction]
techs: [javascript, D3]
designs: [Photoshop]
thumbnail: sample-project/sample.jpg
projectUrl: https://www.sampleorganization.org
testimonial:
  name: Mehran kashefi Sooha
  role: CEO @Example
  image: sample-project/john.jpg
  text: Erkin Company with more than 10 years of experience in designing and executing concrete and metal buildings
---


## Some of the company's executive projects:

**1. The project is a steel-framed building with a welded connection for commercial and office use, which includes 9 floors (2 floors of parking in the basement, including 33 parking lots, a commercial floor in the ground floor and 6 office floors). The total area of the project is 5000 square meters and includes a total of 24 office units.**

AREA: Tehran, Saadat Abad, Darya Blvd. (Employer: Mr. Yousefi)

![sample image](http://www.farsicad.com/wp-content/uploads/2014/10/Nama-Irani-5-www.naghsh-negar.ir-www.farsicad.com_.jpg)

**2. The skeleton type is a concrete building with residential use and has two parking floors and 6 residential floors.Each floor consisted of 2 units of 166 meters, which had a total of 12 residential units and 24 parking lots.The land area was 510 square meters and the total area of the building is 3000 square meters.**

AREA: IRAN, Tehran, Saadat Abad, Khordin Boulevard (Employer: Mr. Fattahieh and Mr. Yousefi)

![sample image](http://www.farsicad.com/wp-content/uploads/2014/10/Nama-Irani-20-www.naghsh-negar.ir-www.farsicad.com_.jpg)

**3. The project is the implementation of a five-storey residential building with one unit and two floors of parking (one in the basement and the other on the ground floor) with a concrete frame system.The dimensions of the land are 151 square meters, which is approximately 100 meters (60% plus 2 meters) It is the foundation of the building we are looking at. The general shape of the building is not rectangular and each face has separate dimensions.**

AREA: IRAN, Tehran, Bagh Feyz, 22 Bahman St. (Employer: Mr. Rezaeizadeh)

![sample image](http://www.farsicad.com/wp-content/uploads/2014/10/Nama-Irani-20-www.naghsh-negar.ir-www.farsicad.com_.jpg)

**3. The project consisted of 9 floors (1 basement floor including play area, 2 parking floors and 6 residential floors). The area of each floor is 180 square meters. And the total area of the building is 1600 square meters. The structure of the building is made of concrete with a bending frame system.**

AREA: IRAN, Terran, Bagh Feyz, Amir Ebrahim St. (Employer: Mr. Javan)

![sample image](http://www.farsicad.com/wp-content/uploads/2014/10/Nama-Irani-20-www.naghsh-negar.ir-www.farsicad.com_.jpg)



