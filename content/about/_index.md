---
title: ABOUT
description: Erkin Company
images: ["/images/sample.jpg"]
---


## Introducing Erkin Tower Builders Company:

Erkin Tower Builders Company has started its work since 2012 with the aim of implementing quality buildings and international standards. The company is engaged in building contract management, reconstruction of dilapidated buildings, as well as contracting various parts of the building, especially in the field of waffle roofs.
Execution of the building with the best quality and use of new construction technologies is one of the company's priorities and time and cost management in all stages of construction is another concern of the company. Providing daily, monthly and general project report (along with a special booklet) to the employer, as well as performing all financial work of the project and providing a transparent report to the employer (financial summary) are other services of Erkin Tower Builders Company.
Erkin Tower Builders Company, with a full team of contractors and experienced workshop engineers, is proud to execute your project in the best possible way.
